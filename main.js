window.graphData = "";
document.getElementById('fileInput').addEventListener('change', function(event) {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = function(e) {
        const content = e.target.result;
        try {
            const jsonData = JSON.parse(content); 
            // displayData(jsonData);
            graphData = JSON.stringify(jsonData);
        } catch (error) {
            console.error('Error parsing JSON:', error);
            document.getElementById('output').innerText = 'Error parsing JSON: ' + error.message;
        }
    };

    // Read the file as text
    reader.readAsText(file);
});

function exportJSON(data, filename) {
    const jsonStr = JSON.stringify(data, null, 2);

    // Create a blob object containing the JSON data
    const blob = new Blob([jsonStr], { type: 'application/json' });

    // Create a download link
    const downloadLink = document.createElement('a');
    downloadLink.href = URL.createObjectURL(blob);
    downloadLink.download = filename;

    // Trigger the download by clicking the link
    downloadLink.click();
}